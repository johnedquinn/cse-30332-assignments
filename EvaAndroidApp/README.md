# EVA Android Application

### Where to find
Please go to `https://gitlab.com/jdimpel/paradigms-androidapp` to view this project.

### Overview
This application is a week-long intoduction into Android development. Its goal is to load in a JSON file from NASA's provided information about EVAs (whether it be maintenance or procedural activities).

### Contact
If you would like access to the project, whether to view or modify, please feel free to reach out to:
- John Dimpel (jdimpel@nd.edu)
- John Quinn (johnedquinn@gmail.com)

The Android Studio Project base folder is EVA_Application.
Path to files:
EVA_Application/app/src/main/java/com/example/eva_application/CustomJSONParser
EVA_Application/app/src/main/java/com/example/eva_application/EVA
EVA_Application/app/src/main/java/com/example/eva_application/MainActivity
EVA_Application/app/src/main/res/values/strings
EVA_Application/app/src/main/res/layouts/activity_main
EVA_Application/app/src/main/res/menu/main
