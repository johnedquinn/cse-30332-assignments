# CSE-30332-ASSIGNMENTS
## John Ed Quinn
This repository contains assignments issued by CSE-30332, the Programming Paradigms course at the University of Notre Dame.
### Content
- PythonPrimer
- WebservicePrimer
### Note
If this repository is currently private and you would like to view the raw code (AKA for recruiting or research purposes), please feel free to email me at `johnedquinn@gmail.com`.
