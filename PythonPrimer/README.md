# PythonPrimer
## Overview
This assignments goes over loading JSON structures from files, modifying subsets of the JSON structures, and performing analysis on the data retrieved.
This is a precursor to the `WebservicePrimer` that will be completed later in the week.
