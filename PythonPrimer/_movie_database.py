#!/usr/bin/env python3

class _movie_database:

	def __init__(self):
		self.movies = {}
		self.users = {}
		self.ratings = {}

	def load_movies(self, movie_file):
		self.movies = {}
		f = open(movie_file)
		for line in f:
			line = line.rstrip()
			components = line.split("::")
			mid = int(components[0])
			mname = components[1]
			mgenres = components[2]

			self.movies[mid] = [mname, mgenres]
		f.close()

	def print_sorted_movies(self):
		for mid, movie in sorted(self.movies.items()):
			print(movie)

	def get_movie(self, mid):
		if mid in self.movies.keys():
			return self.movies[mid]
		return None

	def get_movies(self):
		mids = []
		for mid in sorted(self.movie.keys()):
			mids.append(mid)
		return mids

	def set_movie(self, mid, info):
		self.movies[mid] = info

	def delete_movie(self, mid):
		self.movies.pop(mid, None)

	def load_users(self, users_file):
		self.users = {}
		f = open(users_file)
		for line in f:
			line = line.rstrip()
			components = line.split("::")
			uid = int(components[0])
			ugender = components[1]
			uage = int(components[2])
			uocc = int(components[3])
			uzip = components[4]

			self.users[uid] = [ugender, uage, uocc, uzip]
		f.close()
	
	def get_user(self, uid):
		if uid in self.users.keys():
			return self.users[uid]
		return None

	def get_users(self):
		uids = []
		for uid in sorted(self.movie.keys()):
			uids.append(mid)
		return uids

	def set_user(self, uid, info):
		self.users[uid] = info

	def delete_user(self, uid):
		self.users.pop(uid, None)

	def load_ratings(self, ratings_file):
		f = open(ratings_file)
		ratings = {}
		for line in f:
			line = line.rstrip()
			components = line.split("::")
			uid = int(components[0])
			mid = int(components[1])
			urating = int(components[2])

			if mid in self.ratings:
				self.ratings[mid][uid] = urating
			else:
				self.ratings[mid] = {uid: urating}
		f.close()

	def get_rating(self, mid):
		cumulative = 0
		if mid in self.ratings.keys():
			for rating in self.ratings[mid].values():
				cumulative += rating
			return cumulative / len(self.ratings[mid])
		return None

	#TODO return None if no object data??????????
	def get_highest_rated_movie(self):
		highest_rating = 0
		highest_rated_mid = 0
		for mid in sorted(self.ratings):
			rate = self.get_rating(mid)
			if rate == None:
				continue
			if rate > highest_rating:
				highest_rating = rate
				highest_rated_mid = mid
		return highest_rated_mid

	def set_user_movie_rating(self, uid, mid, rating):
		self.ratings[mid][uid] = rating
	
	def get_user_movie_rating(self, uid, mid):
		if mid not in self.ratings:
			return None
		if uid not in self.ratings[mid]:
			return None
		return self.ratings[mid][uid]

	def delete_all_ratings(self):
		self.ratings = {}

if __name__ == "__main__":
	mdb = _movie_database()

	#### MOVIES ########
	mdb.load_movies('ml-1m/movies.dat')
	mdb.load_users('ml-1m/users.dat')
	mdb.load_ratings('ml-1m/ratings.dat')
	rating = mdb.get_rating(787)
	print(rating)
	mdb.set_user_movie_rating(41, 787, 2)
	rating = mdb.get_rating(787)
	print(rating)

