##
# File: main.py
# Description: service provider
##

import cherrypy
import json

from dictionary_controller import DictionaryController

def start_service():

    dcon = DictionaryController() # object
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    # connect to controller
    dispatcher.connect('dict_get_key', '/dictionary/:key', controller=dcon, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('dict_get_dict', '/dictionary/', controller=dcon, action='GET_DICT', conditions=dict(method=['GET']))
    dispatcher.connect('dict_put_key', '/dictionary/:key', controller=dcon, action='PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('dict_post_pair', '/dictionary/', controller=dcon, action='POST_PAIR', conditions=dict(method=['POST']))
    dispatcher.connect('dict_delete_key', '/dictionary/:key', controller=dcon, action='DELETE_KEY', conditions=dict(method=['DELETE']))
    dispatcher.connect('dict_delete_dict', '/dictionary/', controller=dcon, action='DELETE_DICT', conditions=dict(method=['DELETE']))

    #configuration
    conf = {
            'global' : {
                'server.socket_host' : 'student04.cse.nd.edu',
                'server.socket_port' : 51075,
                },
            '/' : {'request.dispatch' : dispatcher}
            }
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

if __name__ == '__main__':
    start_service()
