##
# File: dictionary_controller.py
# Author: John Ed Quinn
# Description: functions for dealing with dictionary data
##

import json
import cherrypy

class DictionaryController(object):

    def __init__(self):
        self.myd = dict()

    def get_value(self, key):
        value = self.myd[key]
        return value


    # event handlers
    def GET_KEY(self, key):

        # rule 1: set up some default output
        output = {'result': 'success'}

        # rule 2: check your data and type - key and payload
        key = str(key)

        # rule 3: try - except blocks
        try:
            value = self.get_value(key)
            if value is not None:
                output['key'] = key
                output['value'] = value
            else:
                output['result'] = 'error'
                output['message'] = 'None type value associated with requested key'
        except KeyError as ex:
            output['result'] = 'error'
            output['message'] = 'key not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def PUT_KEY(self, key):
        output = {'result':'success'}
        key = str(key)
        #extract msg from body
        data = cherrypy.request.body.read()
        print(data)
        data = json.loads(data)

        try:
            val = data['value']
            self.myd[key] = val # do your local data update
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def GET_DICT(self):

        # rule 1: set up some default output
        output = {}
        output['result'] = 'success'
        output['entries'] = []
        for key, val in self.myd.items():
            output['entries'].append({'key': key, 'value': val})

        # rule 3: try - except blocks
        return json.dumps(output)

    def DELETE_KEY(self, key):

        # rule 1: set up some default output
        output = {}
        output['result'] = 'success'
        key = str(key)
        
        try:
            value = self.get_value(key)
            if value is not None:
                del self.myd[key]
            else:
                output['result'] = 'error'
                output['message'] = 'None type value associated with requested key'
        except KeyError as ex:
            output['result'] = 'error'
            output['message'] = 'key not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        # rule 3: try - except blocks
        return json.dumps(output)

    def DELETE_DICT(self):

        # rule 1: set up some default output
        output = {}
        output['result'] = 'success'
        self.myd = {}

        # rule 3: try - except blocks
        return json.dumps(output)

    def POST_PAIR(self):
        output = {'result':'success'}
        data = cherrypy.request.body.read()
        print(data)
        data = json.loads(data)

        try:
            val = data['value']
            key = str(data['key'])
            self.myd[key] = val # do your local data update
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

