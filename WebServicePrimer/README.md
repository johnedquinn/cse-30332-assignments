Webservice Primer
=================
This assignment uses the Python requests module to practice issuing commands to a web service. It practices getting, converting, modifying, and setting JSON data.

The class web service is hosted at the address: `student04.cse.nd.edu:51001`.

Something to notice is how RESTful services borrow from the object-oriented and functional paradigms. Service functions are intended to be as light as possible, affecting as little state as possible, and saving no “session” information, similar conceptually to functional programming. On the other hand, the service is organized like object-oriented code, where each resource is like an object, and each HTTP command is like calling a method in that object.
