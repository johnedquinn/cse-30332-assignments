#!/usr/bin/env python3

#####
# File : webservice_primer.py
# Author: John Ed Quinn
#####

import requests
import json

## Class: _webservice_primer
## Description: NA
class _webservice_primer:

	## Function: init
	## Description: constructor
	def __init__(self):
		self.SITE_URL = 'http://student04.cse.nd.edu:51001'
		self.MOVIES_URL = self.SITE_URL + '/movies/'
		self.RESET_URL = self.SITE_URL + '/reset/'

	## Function: get_movie
	## Description: gets the json of a specified movie
	def get_movie(self, mid):
		url = self.MOVIES_URL + str(mid)
		r = requests.get(url)
		resp = json.loads(r.content)
		return resp

	## Function: set_movie_title
	## Description: sets the specified movie title to a specified title string
	def set_movie_title(self, mid, title):
		url = self.MOVIES_URL + str(mid)
		movie = self.get_movie(mid)
		movie['title'] = title
		r = requests.put(url, data = json.dumps(movie))
		resp = json.loads(r.content)
		return resp

	## Function: reset_movie
	## Description: resets the movie to the empty dictionary
	def reset_movie(self, mid):
		url = self.RESET_URL + str(mid)
		empty_data = {}
		r = requests.put(url, data = json.dumps(empty_data))
		resp = json.loads(r.content)
		return resp

	## Function: delete_movie
	## Description: deletes the movie entry
	def delete_movie(self, mid):
		url = self.MOVIES_URL + str(mid)
		empty_data = {}
		r = requests.delete(url, data = json.dumps(empty_data))
		resp = json.loads(r.content)
		return resp

## Function: main
## Description: Main driver (used mainly for testing)
if __name__ == '__main__':
	MID = 75
	ws = _webservice_primer()
	print(ws.reset_movie(MID))
	print(ws.get_movie(MID))
